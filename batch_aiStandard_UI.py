# -*- coding: UTF-8 -*-
'''
Author: Jaime Rivera
File: batch_aiStandard_UI.py
Date: 2019.07.26
Revision: 2020.01.13
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: This file provides the interfaces necessary to display a shader batch-creator in Maya

'''

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from PySide2 import QtWidgets
from PySide2 import QtGui
from PySide2 import QtCore

import os
import re

import maya_specifics

reload(maya_specifics)


# UI CONSTANTS
WIDGET_WIDTH = 1100
STRIPE_HEIGHT = 145

AVATAR_SIZE = STRIPE_HEIGHT * 0.62
GEO_BTN_SIZE = (STRIPE_HEIGHT / 145.0) * 60

# ICONS
ICONS_PATH = os.path.dirname(os.path.abspath(__file__)).replace('\\', '/') + '/icons/'

# COLORS
BACKGROUND_COLOR = 'rgb(50, 55, 62)'
GRADIENT = 'qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,' \
           'stop:0 rgb(60, 65, 72),' \
           'stop:1 rgb(40, 45, 52))'

# STYLESHEETS
ROUND_BTN_STYLESHEET = 'QPushButton{border:1px solid gray; border-radius:10px; background:transparent}' \
                       'QPushButton:hover{background:rgb(200, 255, 255, 20)}'

SCROLLBAR_STYLE = "QScrollBar:vertical {{background: {BACKGROUND}; width: 8px;}}" \
                  "QScrollBar::handle:vertical" \
                  "{{border: 0px solid red; border-radius:4px; background: rgb(75,80,89);min-height: 20px;}}" \
                  "QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {{background: none;}}" \
                  "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {{background: none;}}" \
                  "".format(BACKGROUND=BACKGROUND_COLOR)

# NAMING CONSTANTS
BASECOLOR = 'basecolor'
METALLIC = 'metallic'
ROUGHNESS = 'roughness'
NORMAL = 'normal'


class AiBatchCreator(QtWidgets.QWidget):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)

        # CONTROL
        self.advanced_options_showing = True

        # STYLE
        self.setStyleSheet('QWidget{background-color:' + BACKGROUND_COLOR + ';}'
                                                                            'QLineEdit{background:none;}' + ROUND_BTN_STYLESHEET)
        self.setWindowTitle('aiStandardSurface batch creator')
        self.resize(WIDGET_WIDTH, WIDGET_WIDTH * 0.6)
        self.setMinimumWidth(WIDGET_WIDTH * 0.7)

        # WIDGETS
        self.grid = QtWidgets.QGridLayout()
        self.grid.setContentsMargins(20, 20, 20, 20)
        self.setLayout(self.grid)

        self.add_folder_btn = QtWidgets.QPushButton()
        self.add_folder_btn.setText('   Add new shader')
        self.add_folder_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'plus.png'))
        self.add_folder_btn.setStyleSheet('height:35px; font-size:16px; icon-size:24px')
        self.grid.addWidget(self.add_folder_btn, 0, 0, 1, 2)

        self.multiple_from_folder_btn = QtWidgets.QPushButton()
        self.multiple_from_folder_btn.setText('   Create multiple from folder')
        self.multiple_from_folder_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'folder.png'))
        self.multiple_from_folder_btn.setStyleSheet('height:35px; font-size:16px; icon-size:24px')
        self.grid.addWidget(self.multiple_from_folder_btn, 0, 2, 1, 2)

        self.shaders_list = QtWidgets.QListWidget()
        self.shaders_list.setStyleSheet(SCROLLBAR_STYLE)
        self.grid.addWidget(self.shaders_list, 1, 0, 1, 4)

        self.grid.setRowMinimumHeight(2, 15)

        # BIG BUTTON
        self.generate_all_btn = QtWidgets.QPushButton()
        self.generate_all_btn.font().setPointSize(50)
        self.generate_all_btn.setText('   Generate all shaders!')
        self.generate_all_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'rocket.png'))
        self.generate_all_btn.setStyleSheet('height:55px; font-size:20px; icon-size:40px')
        self.grid.addWidget(self.generate_all_btn, 3, 0, 1, 4)

        # CUSTOM OPTIONS
        f = QtWidgets.QFrame()
        f.setFrameStyle(QtWidgets.QFrame.HLine | QtWidgets.QFrame.Sunken)
        self.grid.addWidget(f, 4, 0, 1, 4)

        self.custom_group_btn = QtWidgets.QPushButton()
        self.custom_group_btn.setText('Show advanced options')
        self.custom_group_btn.setIconSize(QtCore.QSize(10, 10))
        self.custom_group_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'triangle_right.png'))
        self.custom_group_btn.setStyleSheet('border:none; Text-align:left; background:none')
        self.grid.addWidget(self.custom_group_btn, 5, 0, 1, 1)

        self.custom_group_frame = QtWidgets.QFrame()
        custom_layout = QtWidgets.QGridLayout()
        self.custom_group_frame.setLayout(custom_layout)
        custom_layout.addWidget(QtWidgets.QLabel('BaseColor regex'), 0, 0, 1, 1)
        self.basecolor_regex = QtWidgets.QLineEdit('^[a-zA-Z_]*_BaseColor.*\..{3,4}')
        self.basecolor_regex.setStyleSheet('height:25; color:lime; font-size:15px')
        custom_layout.addWidget(self.basecolor_regex, 0, 1, 1, 1)
        custom_layout.addWidget(QtWidgets.QLabel('Metallic regex'), 1, 0, 1, 1)
        self.metallic_regex = QtWidgets.QLineEdit('^[a-zA-Z_]*_Metallic.*\..{3,4}')
        self.metallic_regex.setStyleSheet('height:25; color:lime; font-size:15px')
        custom_layout.addWidget(self.metallic_regex, 1, 1, 1, 1)
        custom_layout.addWidget(QtWidgets.QLabel('Roughness regex'), 2, 0, 1, 1)
        self.roughness_regex = QtWidgets.QLineEdit('^[a-zA-Z_]*_Roughness.*\..{3,4}')
        self.roughness_regex.setStyleSheet('height:25; color:lime; font-size:15px')
        custom_layout.addWidget(self.roughness_regex, 2, 1, 1, 1)
        custom_layout.addWidget(QtWidgets.QLabel('Normal regex'), 3, 0, 1, 1)
        self.normal_regex = QtWidgets.QLineEdit('^[a-zA-Z_]*_Normal.*\..{3,4}')
        self.normal_regex.setStyleSheet('height:25; color:lime; font-size:15px')
        custom_layout.addWidget(self.normal_regex, 3, 1, 1, 1)
        self.grid.addWidget(self.custom_group_frame, 6, 0, 1, 4)

        # SETUP
        self.make_connections()
        self.toggle_advanced()
        self.show()

    def make_connections(self):
        self.add_folder_btn.clicked.connect(self.add_a_shader)
        self.multiple_from_folder_btn.clicked.connect(self.add_multiple_from_folder)

        self.generate_all_btn.clicked.connect(self.generate_all)

        self.custom_group_btn.clicked.connect(self.toggle_advanced)

    def add_a_shader(self, source_folder=''):
        basecolor = ''
        basecolor_regex = self.basecolor_regex.text()

        metallic = ''
        metallic_regex = self.metallic_regex.text()

        roughness = ''
        roughness_regex = self.roughness_regex.text()

        normal = ''
        normal_regex = self.normal_regex.text()

        # INPUT
        path = QtWidgets.QFileDialog.getExistingDirectory() if not source_folder else source_folder

        # CHECKS
        if path:
            for elem in os.listdir(path):
                tex = path + '/' + elem
                if os.path.isfile(tex) and '.tx' not in tex:
                    if re.match(basecolor_regex, elem):
                        basecolor = tex
                    elif re.match(metallic_regex, elem):
                        metallic = tex
                    elif re.match(roughness_regex, elem):
                        roughness = tex
                    elif re.match(normal_regex, elem):
                        normal = tex

        if basecolor and metallic and roughness and normal:
            shader_name = os.path.basename(path)
            s = ShaderBox(shader_name, basecolor, metallic, roughness, normal)
            wli = QtWidgets.QListWidgetItem()
            wli.setFlags(QtCore.Qt.ItemIsSelectable)
            wli.setSizeHint(s.size() + QtCore.QSize(0, 15))
            s.parent_wli = wli
            self.shaders_list.addItem(wli)
            self.shaders_list.setItemWidget(wli, s)

    def add_multiple_from_folder(self):
        root_path = QtWidgets.QFileDialog.getExistingDirectory()

        if root_path:
            for elem in os.listdir(root_path):
                dir = root_path + '/' + elem
                if os.path.isdir(dir):
                    self.add_a_shader(dir)

    def generate_all(self):
        for i in range(self.shaders_list.count()):
            item = self.shaders_list.item(i)
            s = self.shaders_list.itemWidget(item)
            s.generate_the_shader()

        # Color management fix
        maya_specifics.color_management_fix()

    def toggle_advanced(self):
        self.advanced_options_showing = not self.advanced_options_showing

        if self.advanced_options_showing:
            self.custom_group_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'triangle_down.png'))
            self.custom_group_frame.show()

        else:
            self.custom_group_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'triangle_right.png'))
            self.custom_group_frame.hide()


class ShaderBox(QtWidgets.QGroupBox):

    def __init__(self, shader_name, basecolor_path, metallic_path, roughness_path, normal_path):
        QtWidgets.QGroupBox.__init__(self)
        self.setObjectName('ShaderBox')

        self.parent_wli = None

        stripe_height = STRIPE_HEIGHT
        self.setFixedHeight(stripe_height)

        self.setTitle(shader_name)
        self.setCheckable(True)

        self.setStyleSheet('QGroupBox{border:2px solid white;'
                           'border-radius:13px;'
                           'background-color:' + GRADIENT + ';}'
                                                            'QGroupBox::title{subcontrol-origin: border;'
                                                            'left: 12px;'
                                                            'top: 10px; padding: 0;}'
                                                            'QPushButton,QFrame,QLabel, QLineEdit{background:none;}')

        self.g = QtWidgets.QGridLayout()
        self.setLayout(self.g)
        self.g.setSpacing(8)

        self.g.setRowMinimumHeight(0, 20)
        self.g.setColumnMinimumWidth(1, 15)

        # SHADER NAME AND PATHS
        self.shader_name = shader_name
        self.basecolor_path = basecolor_path
        self.metallic_path = metallic_path
        self.roughness_path = roughness_path
        self.normal_path = normal_path

        # ATTACHED GEOMETRY
        self.attached_geo = set()

        # ATTACHED GEOMETRY FEEDBACK
        self.feedback_list = QtWidgets.QListWidget()
        self.feedback_list.setWindowTitle('Geometry attached')
        self.feedback_list.setFont(QtGui.QFont('arial', 14))

        # WIDGETS
        self.destroy_btn = QtWidgets.QPushButton()
        self.destroy_btn.setText('X')
        self.destroy_btn.setFixedHeight(20)
        self.destroy_btn.setStyleSheet('QPushButton{border:none; color:white; font-size:16px; Text-align:right;}'
                                       'QPushButton:hover{color:red;}')
        self.destroy_btn.setToolTip('Click here to remove this shader')
        self.destroy_btn.clicked.connect(self.delete_this_shaderbox)
        self.g.addWidget(self.destroy_btn, 0, 6, 1, 1)

        self.shader_avatar = AvatarLabel(parent=self)
        self.g.addWidget(self.shader_avatar, 1, 0, 4, 1)

        self.g.addWidget(QtWidgets.QLabel('BaseColor path:'), 1, 2, 1, 1)
        self.basecolor_qline = QtWidgets.QLineEdit()
        self.basecolor_qline.setReadOnly(True)
        self.basecolor_qline.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        a = QtWidgets.QAction('Remap base color', self.basecolor_qline)
        a.triggered.connect(self.remap_basecolor)
        self.basecolor_qline.addAction(a)
        self.g.addWidget(self.basecolor_qline, 1, 3, 1, 1)

        self.g.addWidget(QtWidgets.QLabel('Metallic path:'), 2, 2, 1, 1)
        self.metallic_qline = QtWidgets.QLineEdit()
        self.metallic_qline.setReadOnly(True)
        self.metallic_qline.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        a = QtWidgets.QAction('Remap metallic', self.metallic_qline)
        a.triggered.connect(self.remap_metallic)
        self.metallic_qline.addAction(a)
        self.g.addWidget(self.metallic_qline, 2, 3, 1, 1)

        self.g.addWidget(QtWidgets.QLabel('Roughness path:'), 3, 2, 1, 1)
        self.roughness_qline = QtWidgets.QLineEdit()
        self.roughness_qline.setReadOnly(True)
        self.roughness_qline.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        a = QtWidgets.QAction('Remap roughness', self.roughness_qline)
        a.triggered.connect(self.remap_roughness)
        self.roughness_qline.addAction(a)
        self.g.addWidget(self.roughness_qline, 3, 3, 1, 1)

        self.g.addWidget(QtWidgets.QLabel('Normal path:'), 4, 2, 1, 1)
        self.normal_qline = QtWidgets.QLineEdit()
        self.normal_qline.setReadOnly(True)
        self.normal_qline.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        a = QtWidgets.QAction('Remap normal', self.normal_qline)
        a.triggered.connect(self.remap_normal)
        self.normal_qline.addAction(a)
        self.g.addWidget(self.normal_qline, 4, 3, 1, 1)

        div = QtWidgets.QFrame()
        div.setFrameStyle(QtWidgets.QFrame.VLine | QtWidgets.QFrame.Sunken)
        self.g.addWidget(div, 1, 4, 4, 1)

        self.attach_geo_btn = QtWidgets.QPushButton()
        self.attach_geo_btn.setFixedSize(QtCore.QSize(GEO_BTN_SIZE, GEO_BTN_SIZE))
        self.attach_geo_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'geo.png'))
        self.attach_geo_btn.setIconSize(QtCore.QSize(GEO_BTN_SIZE - 8, GEO_BTN_SIZE - 8))
        self.attach_geo_btn.setStyleSheet(ROUND_BTN_STYLESHEET)
        self.attach_geo_btn.setToolTip('Make a selection and click here to define the geometry this shader will be'
                                       'applied to.\nIf nothing selected, the geometry list will be emptied.')
        self.attach_geo_btn.clicked.connect(self.set_attached_geo)
        self.g.addWidget(self.attach_geo_btn, 1, 5, 4, 1)

        self.check_geo_btn = QtWidgets.QPushButton()
        self.check_geo_btn.setFixedSize(QtCore.QSize(GEO_BTN_SIZE, GEO_BTN_SIZE))
        self.check_geo_btn.setIcon(QtGui.QIcon(ICONS_PATH + 'list.png'))
        self.check_geo_btn.setIconSize(QtCore.QSize(GEO_BTN_SIZE - 8, GEO_BTN_SIZE - 8))
        self.check_geo_btn.setStyleSheet(ROUND_BTN_STYLESHEET)
        self.check_geo_btn.setToolTip('Click to display the geometry that this shader will be applied to')
        self.check_geo_btn.clicked.connect(self.show_attached_geo)
        self.g.addWidget(self.check_geo_btn, 1, 6, 4, 1)

        # SET PATHS
        self.basecolor_qline.setText(self.basecolor_path)
        self.metallic_qline.setText(self.metallic_path)
        self.roughness_qline.setText(self.roughness_path)
        self.normal_qline.setText(self.normal_path)

    def remap_basecolor(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'New basecolor path')
        if path:
            self.basecolor_qline.setText(path)

    def remap_metallic(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'New metallic path')
        if path:
            self.metallic_qline.setText(path)

    def remap_roughness(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'New roughness path')
        if path:
            self.roughness_qline.setText(path)

    def remap_normal(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'New normal path')
        if path:
            self.normal_qline.setText(path)

    def set_attached_geo(self):
        sel = maya_specifics.get_selection()
        self.attached_geo.clear()
        for elem in sel:
            self.attached_geo.add(elem)

        self.feedback_list.close()
        print self.attached_geo

    def show_attached_geo(self):
        self.feedback_list.clear()
        for elem in self.attached_geo:
            self.feedback_list.addItem(elem)
        self.feedback_list.show()

    def delete_this_shaderbox(self):
        s_list = self.parent_wli.listWidget()
        row = s_list.row(self.parent_wli)
        s_list.takeItem(row)

    def generate_the_shader(self):
        if not self.isChecked():
            return

        maya_specifics.create_ai_shader(self.shader_name,
                                        self.basecolor_path, self.metallic_path, self.roughness_path, self.normal_path,
                                        self.attached_geo)


class AvatarLabel(QtWidgets.QLabel):
    def __init__(self, *args, **kwargs):
        QtWidgets.QLabel.__init__(self, *args, **kwargs)

        self.state = BASECOLOR

        self.Antialiasing = True
        self.setMaximumSize(AVATAR_SIZE, AVATAR_SIZE)
        self.setMinimumSize(AVATAR_SIZE, AVATAR_SIZE)

        self.back = QtGui.QPixmap(self.size())

        self.get_the_path()
        self.draw_the_icon()

        self.reset_tooltip()

    def get_the_path(self):
        if self.state == BASECOLOR:
            return self.parent().basecolor_path
        elif self.state == METALLIC:
            return self.parent().metallic_path
        elif self.state == ROUGHNESS:
            return self.parent().roughness_path
        elif self.state == NORMAL:
            return self.parent().normal_path

    def draw_the_icon(self):
        path = self.get_the_path()

        self.back.fill(QtCore.Qt.transparent)

        pixmap = QtGui.QPixmap(path).scaled(AVATAR_SIZE, AVATAR_SIZE,
                                            QtCore.Qt.KeepAspectRatioByExpanding,
                                            QtCore.Qt.SmoothTransformation)

        painter = QtGui.QPainter(self.back)
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        painter.setRenderHint(QtGui.QPainter.HighQualityAntialiasing, True)
        painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform, True)

        path = QtGui.QPainterPath()
        path.addEllipse(0, 0, self.width(), self.height())

        painter.setClipPath(path)
        painter.drawPixmap(0, 0, pixmap)

        if self.state == BASECOLOR:
            g = QtGui.QRadialGradient()
            g.setColorAt(0, QtGui.QColor(255, 255, 255, 150))
            g.setColorAt(0.2, QtCore.Qt.transparent)
            g.setColorAt(1, QtGui.QColor(0, 0, 0, 180))
            g.setCenter(AVATAR_SIZE / 2, AVATAR_SIZE / 2)
            g.setFocalPoint(AVATAR_SIZE * 0.25, AVATAR_SIZE * 0.25)
            g.setRadius(AVATAR_SIZE)
            painter.fillRect(0, 0, self.width(), self.height(), g)

        self.setPixmap(self.back)

    def mousePressEvent(self, *args, **kwargs):
        if self.state == BASECOLOR:
            self.state = METALLIC
            self.draw_the_icon()
        elif self.state == METALLIC:
            self.state = ROUGHNESS
            self.draw_the_icon()
        elif self.state == ROUGHNESS:
            self.state = NORMAL
            self.draw_the_icon()
        elif self.state == NORMAL:
            self.state = BASECOLOR
            self.draw_the_icon()

        self.reset_tooltip()

    def reset_tooltip(self):
        self.setToolTip('Click on this image to preview the different textures of the material.'
                        '<br>Now showing <b>{}'.format(self.state))